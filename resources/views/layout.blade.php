<!DOCTYPE html>
<html lang="en" data-theme="light">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="shortcut icon" href="{{ asset('logo.png') }}" type="image/x-icon">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@1.0.0/css/bulma.min.css" >
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
        <title>Foro </title>
    </head>
    <body class="container">
        @include('components.navbar')
        <div class="container">
            
            <h1 class="title mt-4"> @yield('titulo') </h1>

            @yield('contenido')
        </div>
    </body>
</html>